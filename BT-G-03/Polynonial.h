﻿#include<iostream>
#include<fstream>
#include<string.h>
using namespace std;

class Polynomial{
	struct Node
	{
		char x;					//Tên của biến
		double mu;				//Hệ số mũ của mỗi biến
		Node *pNext;			//Trỏ đến biến tiếp theo
	};
	struct DonThuc
	{
		double HeSo;			//Hệ số của đơn thức
		Node *Bien;				//1 DSLK chứa tất cả các biến và lũy thừa
		DonThuc*pNext;			//Trỏ đến Đơn thức tiếp theo
	};
	struct DaThuc
	{
		DonThuc* Head;			//DaThuc là dslk trỏ đến đơn thức đầu tiên của đa thức
		DonThuc* Tail;
	};
	DaThuc *poly ;
	char *s;
	DonThuc* Create(const double &HeSo, const char &x, const double &mu){
		DonThuc *d = new DonThuc;
		d->HeSo = HeSo;
		if (mu == 0) d->Bien = NULL;			//mũ vẫn bằng 0 tức là biến đó coi như không có
		if (x == 0) d->Bien = NULL;				//Nếu đơn thức không có biến thì không cần tạo Biến
		else {
			d->Bien = new Node;					//Tạo biến
			d->Bien->x = x;
			d->Bien->mu = mu;
			d->Bien->pNext = NULL;
		}
			d->pNext = NULL;
		return d;
	}
	void atoi(char *s1){
		int length = strlen(s1), a = 1;//a he so nhan neu he so am
		double heso = 0, mu = 0;
		bool flag_heso = true;
		bool flag_mu = true;
		char x = 0;				//Đặt cờ cho biến
		//heso
		int k1 = 10;
		float k2 = 1;
		int j = length;//j: vi tri phan tu tiep theo chua so mu
		if (s1[0] == '-')
			a = -1;
		for (int i = 0; i < length; i++)
		{
			if (s1[i] >= 'a' && s1[i] <= 'z')
				x = s1[i];
			else if (s1[i] >= '0' && s1[i] <= '9')
			{
				heso = heso*k1 + k2*(s1[i] - 48);
				k2 *= k2;
				flag_heso = false;				//Đặt cờ cho hệ số nếu người dùng có nhập hệ số
			}
			else if (s1[i] == '.')
			{
				k1 = 1;
				k2 = 0.1;
			}
			else if (s1[i] == '^')
			{
				j = i + 1;
				break;
			}
			
		}
		heso *= a;
		//mu
		k1 = 10;
		k2 = 1;
		a = 1;
		if (s1[j] == '-')
			a = -1;
		for (; j < length; j++)
		{
			if (s1[j] >= '0' && s1[j] <= '9')
			{
				mu = mu*k1 + k2*(s1[j] - 48);
				k2 *= k2;
				flag_mu = false;				//Tương tự với cờ của hệ số
			}
			else if (s1[j] == '.')
			{
				k1 = 1;
				k2 = 0.1;
			}
		}
		mu *= a;

		if ((heso == 0) && (flag_heso))
		{
			if (s1[0] == '-')
				heso = -1;
			else heso = 1;
		}
		if ((mu == 0)&&(flag_mu)) mu = 1;
		DonThuc* d = Create(heso, x, mu);
		if (poly == NULL)
		{
			poly = new DaThuc;
			poly->Head = poly->Tail = d;
		}
		else if (poly != NULL)
		{
			poly->Tail->pNext = d;
			poly->Tail = d;
		}

	}
	void stoken(char *&token, char*s1, int &startPos){
		strcpy(token, "");
		int length = strlen(s1), count;
		if (startPos == length)
		{
			free(token);
			token = NULL;
			return;
		}
		if (s1[startPos] == '+')
			startPos++;
		count = startPos;
		while (count < length && (s1[count] != '+' && (s1[count] != '-' || s1[count - 1] == '^' || count == startPos)))
			count++;
		int n = count - startPos + 1;
		token = (char*)realloc(token, n);
		strncpy(token, s1 + startPos, n);
		token[n - 1] = '\0';
		startPos = count;
	}
	void removeMu(Node *&t, int k)				//Hàm xóa số mũ bằng k trong đơn thức
	{
		Node *p;
		if (t == NULL) return;
		while (t->mu == k)		//delete head
		{
			Node *tmp = t;
			t = t->pNext;
			delete tmp;
			if (t == NULL) return;
		}
		p = t;
		while (p->pNext != NULL)		//delete body and tail
		{
			while (p->pNext->mu == k)
			{
				Node *tmp = p->pNext;
				p->pNext = p->pNext->pNext;
				delete tmp;
				if (p->pNext == NULL) break;
			}
			p = p->pNext;
			if (p == NULL) break;
		}
	}
	void rutGonDonThuc(DonThuc *&p)					//Cần rút gọn đơn thức để dễ cho việc tình toán
	{
		Node *t = p->Bien;
		while (t != NULL)
		{
			Node *pret = t->pNext;
			while (pret != NULL)
			{
				if (t->x == pret->x)				//Gộp những biến có tên trùng nhau
				{
					t->mu += pret->mu;
					pret->mu = 0;					//Gán số mũ của biến bị trùng thành 0 và dồn mũ lên trước
				}
				pret = pret->pNext;
			}
			t = t->pNext;
		}
		removeMu(p->Bien, 0); //remove Coefficient==0 at p,				// Xóa những biến có số mũ = 0
	}
	void chuanHoaDonThuc(DonThuc *&p)
	{
		Node*i = p->Bien;
		if (i == NULL) return;
		while (i->pNext != NULL)
		{
			Node* j = i->pNext;
			while (j != NULL)
			{
				if (j->x < i->x)
				{
					char tmp = i->x;
					i->x = j->x;
					j->x = tmp;
					double dtmp = i->mu;
					i->mu = j->mu;
					j->mu = dtmp;
				}
				j = j->pNext;
			}
			i=i->pNext;
		}
	}
	void removeHeSo(DaThuc *&t, int k)				//Hàm xóa các đơn thức có hệ số = 0
	{
		DonThuc *p;
		if (t->Head == NULL) return;
		while (t->Head->HeSo == k)		//delete head
		{
			DonThuc *tmp = t->Head;
			t->Head = t->Head->pNext;
			delete tmp;
			if (t->Head == NULL) return;
		}
		p = t->Head;
		while (p->pNext != NULL)		//delete body and tail
		{
			while (p->pNext->HeSo == k)
			{
				DonThuc *tmp = p->pNext;
				p->pNext = p->pNext->pNext;
				delete tmp;
				if (p->pNext == NULL) break;
			}
			p = p->pNext;
			if (p == NULL) break;
		}
		p = t->Head;
		while (p->pNext != NULL)
		{
			p = p->pNext;
		}
		t->Tail = p;
	}
	bool Compare(Node *&bien1, Node *&bien2)
	{
		if (bien1 == NULL && bien2 == NULL)
			return 1;
		else if (bien1 == NULL && bien2 != NULL)
			return 0;
		else if (bien1 != NULL && bien2 == NULL)
			return 0;
		bool flag = true;
		Node *b1 = bien1;
		Node *b2 = bien2;
		while ((b1 != NULL) && (b2 != NULL))
		{
			if ((b1->mu != b2->mu) || (b1->x != b2->x))
			{
				return false;
			}
			b1 = b1->pNext;
			b2 = b2->pNext;
		}
		if ((b1==NULL)&&(b2==NULL))
		return true;
		else return false;
		
	}
	void rutGonDaThuc(DaThuc *&a)			//Rút gọn đa thức gồm : gộp, xóa hệ số = 0
		{
			if (a == NULL) return;
			DonThuc *head = a->Head;
			while (head != NULL)			//Do rutGonDonThuc first so it will be easier to compile later
			{
				rutGonDonThuc(head);		// This function check for (index=0 and remove a variable)&&(any suited variable also compile them)
				chuanHoaDonThuc(head);		//This function duty sort the variables 'a' to 'z'
				head = head->pNext;
			}
			//Luận
			DonThuc *temp = a->Head;
			DonThuc *temp1 = new DonThuc;
			DonThuc *p = new DonThuc;
			while (temp != NULL)
			{
				temp1 = temp->pNext;
				p = temp;
				while (temp1 != NULL)
				{
					if (temp1->HeSo == 0)
					{
						p->pNext = temp1->pNext;
						free(temp1);
						temp1 = p;
					}
					else if (Compare(temp->Bien, temp1->Bien) == 1)
					{
						temp->HeSo += temp1->HeSo;
						p->pNext = temp1->pNext;
						if (temp1 == a->Tail)
							a->Tail = p;
						free(temp1);
						temp1 = p;
					}
					p = temp1;
					temp1 = temp1->pNext;
				}
				temp = temp->pNext;
			}
			removeHeSo(a, 0); 	//remove Coefficient==0 at p
	}
	void swapDonThuc(DonThuc *&a, DonThuc *&b)
	{
		double dtmp = b->HeSo;
		b->HeSo = a->HeSo;
		a->HeSo = dtmp;
		Node*tmp = b->Bien;				//Đổi địa chỉ 2 con trỏ quản lí biến và mũ
		b->Bien = a->Bien;
		a->Bien = tmp;
	}
	double tongMuDonThuc(DonThuc *&p)
	{
		Node* head = p->Bien;
		double tmp = 0;
		while (head != NULL)
		{
			tmp += head->mu;
			head = head->pNext;
		}
		return tmp;
	}
	void chuanHoaDaThuc(DaThuc *&p)
	{
		if (p == NULL) return;
		if (p->Head == NULL) return;
		DonThuc* i = p->Head;
		while (i->pNext != NULL)
		{
			DonThuc* j = i->pNext;
			while (j != NULL)
			{
				double tong_i= tongMuDonThuc(i), tong_j= tongMuDonThuc(j);
				if (tong_j > tong_i)
				{
					swapDonThuc(i, j);
				}
				else if ((tong_j == tong_i)&&(i->Bien->x>j->Bien->x))
				{
					swapDonThuc(i, j);
				}
				j = j->pNext;
			}
			i = i->pNext;
		}
	}
	DonThuc* multiplyDonThuc( DonThuc *a,  DonThuc *&b)			//Hàm nhân 2 đơn thức
		{
			DonThuc *tmp = new DonThuc;
			tmp->Bien = NULL;
			tmp->HeSo = a->HeSo*b->HeSo;
			Node* ptmp = tmp->Bien;
			Node* p = a->Bien;
			while (p!= NULL)
			{
				if (tmp->Bien == NULL)							//add to tmp
				{
					tmp->Bien = new Node;
					tmp->Bien->x = p->x;
					tmp->Bien->mu = p->mu;
					tmp->Bien->pNext = NULL;
					ptmp = tmp->Bien;
				}
				else {
					Node* tmptmp = new Node;
					tmptmp->x = p->x;
					tmptmp->mu = p->mu;
					tmptmp->pNext = NULL;
					ptmp->pNext = tmptmp;
					ptmp = ptmp->pNext;
				}
				p = p->pNext;
			}
			p = b->Bien;
			while (p != NULL)
			{
				if (tmp->Bien == NULL)							//add to tmp
				{
					tmp->Bien = new Node;
					tmp->Bien->x = p->x;
					tmp->Bien->mu = p->mu;
					tmp->Bien->pNext = NULL;
					ptmp = tmp->Bien;
				}
				else {
					Node* tmptmp = new Node;
					tmptmp->x = p->x;
					tmptmp->mu = p->mu;
					tmptmp->pNext = NULL;
					ptmp->pNext = tmptmp;
					ptmp = ptmp->pNext;
				}
				p = p->pNext;
			}
			tmp->pNext = NULL;
			rutGonDonThuc(tmp);
			return tmp;
		}
public:																			//Phương thức
	Polynomial()
	{
		poly = NULL;
		s = new char[1000];
	}
	~Polynomial()
	{
		delete[] s;
		if (poly != NULL)
		{
			while (poly->Head != NULL)
			{
				while (poly->Head->Bien != NULL)
				{
					Node *tmp = poly->Head->Bien;
					poly->Head->Bien = poly->Head->Bien->pNext;
					delete tmp;
				}
				DonThuc*tmp = poly->Head;
				poly->Head = poly->Head->pNext;
				delete tmp;
			}
			poly = NULL;
		}
	}
	void addTail(DonThuc *d)					//Phương thức thêm 1 đơn thức đã có vào đa thức
	{
		DonThuc *tmp = new DonThuc;				//Biến clone của Đơn thức cần truyền vào
		tmp->HeSo = d->HeSo;
		tmp->Bien = NULL;
		Node *p = d->Bien;
		Node *head;
		while (p != NULL)						//Tạo chính xác bản clone
		{
			Node *bien_tmp = new Node;
			bien_tmp->mu = p->mu;
			bien_tmp->x = p->x;
			bien_tmp->pNext = NULL;
			if (tmp->Bien == NULL)
			{
				tmp->Bien = bien_tmp;
				head = tmp->Bien;
			}
			else {
				head->pNext = bien_tmp;
			}
			p = p->pNext;
		}
		tmp->pNext = NULL;
		if (poly == NULL)
		{
			poly = new DaThuc;
			poly->Head = poly->Tail = tmp;				//trỏ đến địa chỉ của bản clone
		}
		else if (poly != NULL)
		{
			poly->Tail->pNext = tmp;
			poly->Tail = tmp;
		}
	}
	void rutGon()										//Phương thức rút gọn đa thức
	{
		rutGonDaThuc(poly);
	}
	void chuanHoa()
	{
		chuanHoaDaThuc(poly);
	}
	friend istream &operator >>(istream& in, Polynomial &p)			//Phương thức nhập đa thức, có tích hợp rút gọn hệ số
	{
		double HeSo = 0, mu = 1;
		char x;
		int n = 10;
		float k = 1;
		int i;
		for (i = 0; (i < 255) && (!in.eof()); i++)
			in >> p.s[i];
		p.s[i - 1] = '\0';
		char *temp = new char[strlen(p.s) + 1];
		strcpy(temp, p.s);
		char *dt = new char;
		int start = 0;
		while (dt != NULL)
		{
			p.stoken(dt, temp, start);
			if (dt != NULL)
				p.atoi(dt);
		}
		p.rutGon();
		return in;
	}
	friend ostream &operator <<(ostream& out, const Polynomial &p)			//Phương thức xuất đa thức
	{
		if (p.poly == NULL) return out << "0";
		DonThuc* tmp = p.poly->Head;
		while (tmp != NULL)
		{
			Node* tmpBien = tmp->Bien;
			if (tmpBien != NULL)								//Nếu có biến ở đằng sau
			{
				if (tmp == p.poly->Head)						//Xử lí xuất sao cho đẹp, tránh trường hợp hệ số = 1 
				{
					if (tmp->HeSo == -1.0)
						out << "-";
					else if (tmp->HeSo != 1.0) out << tmp->HeSo;
				}
				else
				{
					if (abs(tmp->HeSo) == 1)
					{
						if (tmp->HeSo == -1.0)
							out << "-";
						else if (tmp->HeSo == 1.0) out << "+";
					}
					else
						if (tmp->HeSo > 0)
							out << "+" << tmp->HeSo;
						else out << tmp->HeSo;
				}
			}
			else {												//Nếu không có biến mà chỉ có hệ số
				if (tmp == p.poly->Head)						//Xử lí xuất sao cho đẹp, tránh trường hợp hệ số = 1 
				{
					if (tmp->HeSo == -1.0)
						out << "-1";
					else if (tmp->HeSo != 1.0) out << tmp->HeSo;
				}
					else 	if (tmp->HeSo > 0)
							 out << "+" << tmp->HeSo;
							else out << tmp->HeSo;
			}
			while (tmpBien != NULL)				
			{
				if (abs(tmp->HeSo)!=1) out << "*";	
				out << tmpBien->x;								//Xử lí hệ số và số mũ
				if (tmpBien->mu!=1)
					out<< "^" << tmpBien->mu;
				tmpBien = tmpBien->pNext;
			}
			tmp = tmp->pNext;
		}
		if (p.poly->Head == NULL)
			out << "0";
		return out;
	}
	Polynomial& operator=(Polynomial right)
	{
		//Hủy trước khi gán
		if (poly!=NULL)
				delete this;
		if (right.poly != NULL)
		{
			DonThuc*first = right.poly->Head;
			
			while (first != NULL)
			{
				addTail(first);
				first = first->pNext;
			}
		}
		return *this;
	}
	Polynomial& operator+(const Polynomial &right)
	{
		Polynomial *tmp=new Polynomial;
		DonThuc* first = poly->Head;
		DonThuc* second = right.poly->Head;
		while (first != NULL)
			{
				tmp->addTail(first);				//Thềm đa thức con First vào tmp
				first = first->pNext;
			}
		while (second != NULL)
			{
				tmp->addTail(second);				//Thềm đa thức con Second vào tmp
				second = second->pNext;
			}
		rutGonDaThuc(tmp->poly);					//Rút gọn tmp
		chuanHoaDaThuc(tmp->poly);					//Chuẩn hóa tmp
		return *tmp;
	}
	Polynomial& operator-(const Polynomial &right)
	{
		Polynomial *tmp=new Polynomial;
		DonThuc* first = poly->Head;
		DonThuc* second = right.poly->Head;
		while (first != NULL)
		{
			tmp->addTail(first);				//Thềm đa thức con First vào tmp
			first = first->pNext;
		}
		while (second != NULL)
		{
			second->HeSo = -second->HeSo;
			tmp->addTail(second);				//Thềm đa thức con Second vào tmp
			second->HeSo = -second->HeSo;		//Trả lại dấu -
			second = second->pNext;
		}
		rutGonDaThuc(tmp->poly);				//Rút gọn tmp
		chuanHoaDaThuc(tmp->poly);				//Chuẩn hóa tmp
		return *tmp;
	}
	Polynomial& operator*(const Polynomial &right)						//Phương thức nhân 2 đa thức 
	{
		Polynomial *tmp=new Polynomial;
		DonThuc* first = poly->Head;
		while (first != NULL)
		{
			DonThuc* second = right.poly->Head;
			while (second != NULL)
			{
				tmp->addTail(multiplyDonThuc(first, second));			//Thềm đơn thức con vào tmp
				second = second->pNext;
			}
			first = first->pNext;
		}
		rutGonDaThuc(tmp->poly);										//Rút gọn tmp
		chuanHoaDaThuc(tmp->poly);										//Chuẩn hóa tmp
		return *tmp;
	}
};